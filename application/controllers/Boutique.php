<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boutique extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->data['titrePage'] = '';
		
		//Liens
		
		//Modèle
		$this->load->model('', '');
		$this->load->model('', '');

		//Librairies
		//$this->load->library('fonctions');		
		//$this->load->library('');

		//$this->lang->load('content');
		
		//menu
		//$this->data['menu'] = $this->fonctions->menuPublic;
		//$this->data['menu']['accueil'] = 'current';
		
		//langue courante		
		//$this->lg = $this->lang->lang();
		//$this->data['lang'] = $this->lg;
		
		//$this->output->enable_profiler(TRUE);
	}

	public function index()
	{
		$this->load->view('boutique');
	}
}
