<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<title>Biblio+, Bibliothèque et Formations en ligne</title>
<meta name="author" content="tansh">
<meta name="description" content="HTML Teamplate">
<meta name="keywords" content="Bibliothèque, Formations">

<!-- FAVICON FILES -->
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon" sizes="144x144">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-120-precomposed.png" rel="apple-touch-icon" sizes="120x120">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-76-precomposed.png" rel="apple-touch-icon" sizes="76x76">
<link href="<?php echo base_url(); ?>assets/images/icons/favicon.png" rel="shortcut icon">

<!-- CSS FILES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/iconfonts.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color-blue.css">
</head>
<body>
<div id="dtr-wrapper" class="clearfix"> 
    
    <!-- preloader starts -->
    <div class="dtr-preloader">
        <div class="dtr-preloader-inner">
            <div class="dtr-preloader-img"></div>
        </div>
    </div>
    <!-- preloader ends --> 
    
    <!-- Small Devices Header 
============================================= -->
    <div class="dtr-responsive-header fixed-top">
        <div class="container"> 
            
            <!-- small devices logo --> 
            <a href="index.html"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
            <!-- small devices logo ends --> 
            
            <!-- menu button -->
            <button id="dtr-menu-button" class="dtr-hamburger" type="button"><span class="dtr-hamburger-lines-wrapper"><span class="dtr-hamburger-lines"></span></span></button>
        </div>
        <div class="dtr-responsive-header-menu"></div>
    </div>
    <!-- Small Devices Header ends 
============================================= --> 
    
    <!-- Header 
============================================= -->
    <header id="dtr-header-global" class="fixed-top">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between"> 
                
                <!-- header left starts -->
                <div class="dtr-header-left"> 
                    
                    <!-- logo --> 
                    <a class="logo-default dtr-scroll-link" href="#home"><img src="<?php echo base_url(); ?>assets/images/logo-light.png" alt="logo"></a> 
                    
                    <!-- logo on scroll --> 
                    <a class="logo-alt dtr-scroll-link" href="#home"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
                    <!-- logo on scroll ends --> 
                    
                </div>
                <!-- header left ends --> 
                
                <!-- menu starts-->
                <div class="main-navigation navbar navbar-expand-lg ml-auto">
                    <ul class="dtr-scrollspy navbar-nav dtr-nav light-nav-on-load dark-nav-on-scroll">
                        <li class="nav-item"> <a class="nav-link active" href="<?php echo base_url(); ?>#home">Accueil</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#about">Qui sommes-nous?</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#features">Passer à l'action</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#news">News</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('boutique'); ?>">La boutique</a> </li>
                    </ul>
                </div>
                <!-- menu ends--> 
                
                <!-- header right starts -->
                <div class="dtr-header-right"> <a href="#donation" class="dtr-btn btn-rounded btn-orange dtr-scroll-link">S'abonner</a> </div>
                <!-- header right ends --> 
                
            </div>
        </div>
    </header>
    <!-- header ends
================================================== --> 
    
    <!-- == main content area starts == -->
    <div id="dtr-main-content"> 
        
        <!-- hero section starts
================================================== -->
        <section id="home" class="dtr-section hero-section-top-padding dtr-section-with-bg dtr-pb-100" style="background-image: url(<?php echo base_url(); ?>assets/images/hero-img3.jpg);">>
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 offset-md-6">
                        <div class="dtr-box dtr-rounded bg-white">
                            <h2 class="text-center">S'abonner</h2>
                            <p class="text-center text-grad-orange font-weight-700">à la bibliothèque en ligne</p>
                            
                            <!-- form starts -->
                            <div class="dtr-form">
                                <form id="contactform" method="post" action="php/contact-form.php">
                                    <fieldset>
                                        <div class="dtr-form-row dtr-form-row-2col">
                                            <p class="dtr-form-column">
                                                <input name="name"  type="text" placeholder="Nom(s)">
                                            </p>
                                            <p class="dtr-form-column">
                                                <input name="phone"  type="text" placeholder="Téléphone">
                                            </p>
                                        </div>
                                        <p>
                                            <input name="email"  class="required email" type="text" placeholder="Email">
                                        </p>
                                        <p>
                                            <select name="abonnement"  class="required" type="text" placeholder="Selectionnez votre formule">
                                                <option value="1">55 000 FCFA/An - Personnes individuelles</option>
                                                <option value="2">60 000 FCFA/An - Pour la lecture guidée</option>
                                                <option value="3">28 000 FCFA/An - Pour les écoles et universités</option>
                                            </select>
                                        </p>
                                        <p class="antispam">Laissez ceci vide: <br />
                                            <input name="url" />
                                        </p>
                                        <p>
                                            <textarea rows="6" name="message" id="message" class="required"  placeholder="Message"></textarea>
                                        </p>
                                        <p class="text-center">
                                            <button class="dtr-btn btn-rounded btn-blue" type="submit">Envoyer</button>
                                        </p>
                                        <div id="result"></div>
                                    </fieldset>
                                </form>
                            </div>
                            <!-- form ends -->
                            <p class="text-size-xs font-weight-600 text-center dtr-mt-20">* Vos informations personnelles sont confidentielles. <br>
                                Consultez notre politique de confidentialité pour plus d'informations. </p>
                        </div>
                    </div>
                    <!-- column 1 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- hero section ends
================================================== --> 
        
        <!-- section starts
================================================== -->
        <section id="about" class="dtr-section dtr-pt-100 dtr-pb-80 bg-blue">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <p class="text-size-xl color-white">Biblio+ est une librairie physique et numérique. Biblio+ Vous prête des livres pour lecture et les récupère après votre lecture avec son service de livraison à domicile. </p>
                        <span class="vert-line mt-20"></span>
                        <h2 class="color-white">Réjoignez-nous</h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- section ends
================================================== --> 
        
        <!-- section starts
================================================== -->
        <section class="dtr-section dtr-py-100">
            <div class="container"> 
                
                <!-- circle bg icon -->
                <div class="dtr-circle-icon bg-white color-orange"><i class="icon-mbri-down"></i></div>
                
                <!-- row 1 starts -->
                <div class="row align-items-center"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 dtr-md-mb-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img1.jpg" alt="image"> </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">Accès</span> à tout type de livres</p>
                        </div>
                        <p>Biblio+ vous offre des livres dans diverses catégories. Des livres apdatés à tout type de métier. Nous vous permettons de commander vos livres en ligne et vous êtes livré partout où vous voulez. Notre service est accessible 24/7.</p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 1 ends --> 
                
                <!-- row 2 starts -->
                <div class="row align-items-center dtr-py-100"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">Pour tous type d'age</span> (Enfant, Jeune, Adulte)</p>
                        </div>
                        <p>Nous avons des livres de jeunesse (0 à 3ans), des écoliers, des collégiens, des lycéens, des universaitaires et des travailleurs. </p>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 text-right dtr-md-mt-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img2.jpg" alt="image"> </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 2 ends --> 
                
                <!-- row 3 starts -->
                <div class="row align-items-center"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 dtr-md-mb-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img3.jpg" alt="image"> </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">Coût très bas</span> du service</p>
                        </div>
                        <p>Abonnement annuel le plus bas du marché. Nous vous proposons l'abonnement à moindre coût avec la possibilité d'échelonner le paiement de votre abonnement annuel. Votre abonnement comprend la commande de livres et leurs livraisons.</p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 3 ends --> 
                
            </div>
        </section>
        <!-- section ends
================================================== --> 
        
        <!-- hashtag section starts
================================================== -->
        <section class="dtr-section hashtag-section-padding dtr-section-with-bg parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg.jpg);">
            <div class="dtr-overlay bg-dark-blue-trans"></div>
            <div class="container dtr-overlay-content">
                <div class="row">
                    <div class="col-12">
                        <h5 class="dtr-mb-30 color-white">Ensemble nous aurons le ...</h5>
                        <p class="hashtag-heading color-white font-weight-700 wow fadeInUp">#plaisir<span class="color-orange">delire</span></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- hashtag section ends
================================================== --> 
        
        <!-- features section starts
================================================== -->
        <section id="features" class="dtr-section dtr-py-100">
            <div class="container"> 
                
                <!-- section intro row starts -->
                <div class="row dtr-mb-30">
                    <div class="col-12 text-center">
                        <p class="text-grad-orange font-weight-700">Des livres de diverses horizons</p>
                        <h2>Des catégories variées</h2>
                    </div>
                </div>
                <!-- section intro row ends --> 
                
                <!-- row 1 starts -->
                <div class="row">
                    <div class="col-12 col-md-2 offset-md-1 text-center dtr-pt-100 dtr-mt-120 dtr-md-mt-0 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="dtr-mb-30">
                        <h6>Développement personnel</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="dtr-mb-30">
                        <h6>Livre scolaire</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-pt-20 dtr-md-mt-0"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="dtr-mb-30">
                        <h6>Religieux</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="dtr-mb-30">
                        <h6>Théatre & Poésie</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-pt-100 dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon-64x64.png" alt="image" class="dtr-mb-30">
                        <h6>Livres pour enfant</h6>
                    </div>
                </div>
                <!-- row 1 ends --> 
                
                <!-- row 2 starts -->
                <div class="row dtr-section-with-bg dtr-bg-size-auto dtr-bg-position-center-top dtr-mt-minus110 dtr-md-mt-30 wow fadeInUp" style="background-image: url(assets/images/curve.svg);">
                    <div class="col-12 col-md-6 offset-md-3 text-center dtr-pt-50">
                        <p class="dtr-mb-0 color-dark-blue">Soyez prêt à voyager avec</p>
                        <p class="text-size-md-alt font-weight-600 dtr-mb-20 color-orange">#biblioplus</p>
                        <p class="color-dark-blue"> Si vous avez besoin d'un livre,<br>
                            quelques soit l'endroit ou vous trouvez</p>
                        <p class="font-weight-700 dtr-mt-20 color-blue">N'hesitez pas à nous contacter! </p>
                        <p class="text-size-xs font-weight-600 dtr-mt-30 color-dark-blue">Pour consulter les livres disponible chez #BIBLIOPLUS:<br>
                            www.biblio-plus.com/BOUTIQUE</p>
                    </div>
                </div>
                <!-- row 2 ends --> 
                
            </div>
        </section>
        <!-- features section ends
================================================== --> 
        
        <!-- testimonial section starts
================================================== -->
        <section class="dtr-section dtr-section-with-bg dtr-py-100 parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg1.jpg);"> 
            
            <!-- overlay -->
            <div class="dtr-overlay bg-dark-blue-trans"></div>
            <div class="container dtr-overlay-content"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6"> 
                        
                        <!-- heading-->
                        <h2 class="dtr-mb-30 color-white">Que dit<br>
                            nos lecteurs ...</h2>
                        
                        <!--===== testimonial slider starts =====-->
                        <div class="dtr-slick-slider dtr-testimonial-style1"> 
                            
                            <!--== slide 1 starts ==-->
                            <div class="dtr-testimonial"> 
                                
                                <!-- review text -->
                                <div class="dtr-testimonial-content color-white">“Biblio+ m'a permis de rediger mon memoire facilement.” </div>
                                <!-- client info -->
                                <div class="d-flex align-items-center dtr-client-info color-white">
                                    <div class="dtr-testimonial-user"> <img src="assets/images/icon-64x64.png" alt="image"> </div>
                                    <div>
                                        <h6 class="dtr-client-name">Mlle Aminata Camara</h6>
                                        <span class="dtr-client-job">Etudiante, Abidjan</span></div>
                                </div>
                            </div>
                            <!--== slide 1 ends ==-->
                            
                        </div>
                        <!--===== testimonial slider ends =====--> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-5 offset-md-1 dtr-md-mt-30"> 
                        
                        <!-- video box starts -->
                        <div class="dtr-video-box"> 
                            <!-- image --> 
                            <img src="<?php echo base_url(); ?>assets/images/video-bg.png" alt="image" class="dtr-rounded-big"> 
                            <!-- video button starts -->
                            <div class="color-white"> <a class="dtr-video-popup dtr-video-button bg-orange-trans" data-autoplay="true" data-vbtype="video" href="#"></a> </div>
                            <!-- video button ends --> 
                        </div>
                        <!-- video box ends --> 
                        
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- testimonial section ends
================================================== --> 
        
        <!-- info section starts
================================================== -->
        <section class="dtr-section dtr-py-100 bg-grey">
            <div class="container"> 
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <div class="dtr-pr-30 dtr-sm-pr-0"><img src="<?php echo base_url(); ?>assets/images/image-1.jpg" alt="image"></div>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 dtr-md-mt-30">
                        <p class="text-size-lg font-weight-200 color-blue">vous avez besoin de  <span class="font-weight-600 color-orange">d'accompagnement </span> et de suivi </p>
                        <p class="text-grad-orange font-weight-700 dtr-mb-20">Nous mettons une équipe à votre disponibilité</p>
                        <p>Nous sommes là pour vous accompagner tout au long de ce processus, afin que votre investissement réponde à vos besoins et votre situation.</p>
                        <p>Nous sommes là justement pour vous accompagner et vous guider vers les plus spectaculaires. </p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row starts --> 
                
            </div>
        </section>
        <!-- info section ends
================================================== --> 
        
        <!-- services section starts
================================================== -->
        <section class="dtr-section dtr-section-with-bg dtr-py-100 parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg2.jpg);"> 
            
            <!-- overlay -->
            <div class="dtr-overlay bg-blue-trans"></div>
            <div class="container dtr-overlay-content">
                <h2 class="text-center dtr-mb-40 color-white">Nos formules d'abonnement</h2>
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-4"> 
                        
                        <!-- icon box 1 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-users"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">55 000 FCFA/an</h4>
                                <p class="color-white">Pour les travailleurs ou personnes individuelles.</p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">S'abonner</a> </div>
                        <!-- icon box 1 ends --> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- icon box 2 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-shopping-bag"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">60 000 FCFA/AN</h4>
                                <p class="color-white">Pour la lecture guidée.<br><br></p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">S'abonner</a> </div>
                        <!-- icon box 2 ends --> 
                        
                    </div>
                    <!-- column 2 ends --> 
                    
                    <!-- column 3 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- icon box 3 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-alert"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">28 000 FCFA/AN</h4>
                                <p class="color-white">Pour les écoles et universités.<br><br></p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">S'abonner</a> </div>
                        <!-- icon box 3 ends --> 
                        
                    </div>
                    <!-- column 3 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- services section ends
================================================== --> 
        
        <!-- faq section starts
================================================== -->
        <section id="faq" class="dtr-section dtr-py-100 bg-grey">
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <p class="text-size-lg font-weight-200 color-blue">Les <span class="font-weight-600 color-orange">reponses</span> à vos questions</p>
                        <p class="text-grad-orange font-weight-700 dtr-mb-40">Comment utiliser le service Biblio+</p>
                        
                        <!-- accordion 1 starts -->
                        <div class="dtr-accordion accordion" id="accord-index1"> 
                            
                            <!-- accordion tab 1 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading1">
                                    <h5 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link" type="button" data-toggle="collapse" data-target="#accord-index1-collapse1" aria-expanded="true" aria-controls="accord-index1-collapse1">Où trouve-t-on Biblio+?</button>
                                    </h5>
                                </div>
                                <div id="accord-index1-collapse1" class="collapse show" aria-labelledby="accord-index1-heading1" data-parent="#accord-index1">
                                    <div class="card-body">Biblio+ est une bibliothèque en ligne. Nous vous permettons de vous abonner à notre service, de choisir les livres que vous voulez et de vous les faire livrer à l'endroit où vous le souhaitez. Nous vous permettons d'accéder à des milliers de livres sans vous déplacer.</div>
                                </div>
                            </div>
                            <!-- accordion tab 1 ends -->
                            
                            <!-- accordion tab 2 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading2">
                                    <h2 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button" data-toggle="collapse" data-target="#accord-index1-collapse2" aria-expanded="false" aria-controls="accord-index1-collapse2">Comment s'abonner sur Biblio+?</button>
                                    </h2>
                                </div>
                                <div id="accord-index1-collapse2" class="collapse" aria-labelledby="accord-index1-heading2" data-parent="#accord-index1">
                                    <div class="card-body">Pour vous abonner à Biblio+ c'est très facile, il suffit de suivre les instructions suivantes:
                                    <br>
                                    1. Remplir le formulaire d'abonnement
                                    <br>
                                    2. Choisir sa formule d'abonnement
                                    <br>
                                    3. Un email de confirmation vous ai envoyé avec le lien direct à la boutique. Vous pouvez choisir vos livres et vous serrez livré.
                                    </div>
                                </div>
                            </div>
                            <!-- accordion tab 2 ends -->
                            
                            <!-- accordion tab 3 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading3">
                                    <h2 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button" data-toggle="collapse" data-target="#accord-index1-collapse3" aria-expanded="false" aria-controls="accord-index1-collapse3">Quels sont les types de livre qu'offre Biblio+?</button>
                                    </h2>
                                </div>
                                <div id="accord-index1-collapse3" class="collapse" aria-labelledby="accord-index1-heading3" data-parent="#accord-index1">
                                    <div class="card-body">Nouus avons plus de 5 000 livres physiques
                                        <br>Littérature romanesque, poésie, théâtre, ...
                                        <br>Bibliothèque chrétienne
                                        <br>Bibliothèque musulmane
                                        <br>Documents pour les thèses de formation professionnelle
                                        <br>Développement personnel
                                    </div>
                                </div>
                            </div>
                            <!-- accordion tab 3 ends -->
                            
                        </div>
                        <!-- accordion 1 ends -->
                        
                    </div>
                    <!-- column 1 ends -->
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 text-right dtr-md-mt-30"> <img src="<?php echo base_url(); ?>assets/images/image-2.jpg" alt="image"> </div>
                    <!-- column 2 ends -->
                    
                </div>
                <!-- row ends -->
                
            </div>
        </section>
        <!-- faq section ends
================================================== --> 

        <!-- subscribe section starts
================================================== -->
        <section id="subscribe" class="dtr-section dtr-py-50">
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <p class="text-size-lg font-weight-300 color-blue">Obtenez toutes les actualités sur <span class="font-weight-600 color-orange">Biblio+</span> dans votre email. </p>
                    </div>
                    <!-- column 1 starts --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-5 offset-md-1 dtr-md-mt-30"> 
                        
                        <!-- form starts -->
                        <div class="subscribe-form-wrapper">
                            <form id="subscribeform" method="post" action="#">
                                <fieldset>
                                    <div class="subscribe-form-email">
                                        <input name="email"  class="required email" type="email" placeholder="Votre adresse email">
                                        <p class="subscribe-form-submit">
                                            <button type="submit" class="dtr-btn btn-rounded btn-blue"> Souscrire </button>
                                        </p>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- form ends -->
                        
                        <p class="text-size-xs-alt font-weight-400 dtr-mt-5 color-dark-blue">* Vos informations personnelles sont confidentielles. Consultez notre politique de confidentialité. </p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- subscribe section ends
================================================== --> 
        
        <!-- footer section starts
================================================== -->
        <footer id="dtr-footer">
            <div class="container">
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-sm-6 col-lg-3 dtr-mb-30"> <a href="index.html" class="d-block dtr-mb-30"><img src="assets/images/logo.png" alt="footer logo"></a>
                        <div class="dtr-social-large">
                            <ul class="dtr-social dtr-social-list social-light text-left">
                                <li><a href="#" class="dtr-linkedin color-grey" target="_blank" title="linkedin"></a></li>
                                <li><a href="#" class="dtr-facebook color-grey " target="_blank" title="facebook"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-sm-6 col-lg-5 dtr-mb-30">
                        <h6>Biblio+</h6>
                        <p>Vous prête des livres pour lecture et les récupère après votre lecture avec son service de livraison à domicile ...</p>
                        <p class="text-size-xs dtr-mt-20 dtr-mb-30">copyright© 2020 by <a href="http://t7ch.com" target="_blank">t7ch</a>.<br>
                            All rights reserved.</p>
                    </div>
                    <!-- column 2 ends --> 
                     
                    
                    <!-- column 4 starts -->
                    <div class="col-12 col-sm-6 col-lg-4 dtr-mb-30">
                        <h6>Contactez-nous</h6>
                        <p class="d-flex dtr-mb-20"><i class="icon-envelope1 dtr-mt-5 dtr-mr-15"></i><span><a href="mailto:contact@example.com">info@biblioplus.com</a></span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-phone-alt dtr-mt-5 dtr-mr-15"></i><span>+225 47 24 66 59</span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-map-marker-alt dtr-mt-5 dtr-mr-15"></i><span>Riviera 2,
                            Abidjan,<br>
                            Côte d'Ivoire</span></p>
                    </div>
                    <!-- column 4 ends --> 
                    
                </div>
            </div>
        </footer>
        <!-- footer section ends
================================================== --> 
        
    </div>
    <!-- == main content area ends == --> 
    
</div>
<!-- #dtr-wrapper ends --> 

<!-- JS FILES --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>