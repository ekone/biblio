<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<title>Biblio+, Bibliothèque et Formations en ligne</title>
<meta name="author" content="tansh">
<meta name="description" content="HTML Teamplate">
<meta name="keywords" content="Bibliothèque, Formations">

<!-- FAVICON FILES -->
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon" sizes="144x144">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-120-precomposed.png" rel="apple-touch-icon" sizes="120x120">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-76-precomposed.png" rel="apple-touch-icon" sizes="76x76">
<link href="<?php echo base_url(); ?>assets/images/icons/favicon.png" rel="shortcut icon">

<!-- CSS FILES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/iconfonts.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color-blue.css">
</head>
<body>
<div id="dtr-wrapper" class="clearfix"> 
    
    <!-- preloader starts -->
    <div class="dtr-preloader">
        <div class="dtr-preloader-inner">
            <div class="dtr-preloader-img"></div>
        </div>
    </div>
    <!-- preloader ends --> 
    
    <!-- Small Devices Header 
============================================= -->
    <div class="dtr-responsive-header fixed-top">
        <div class="container"> 
            
            <!-- small devices logo --> 
            <a href="index.html"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
            <!-- small devices logo ends --> 
            
            <!-- menu button -->
            <button id="dtr-menu-button" class="dtr-hamburger" type="button"><span class="dtr-hamburger-lines-wrapper"><span class="dtr-hamburger-lines"></span></span></button>
        </div>
        <div class="dtr-responsive-header-menu"></div>
    </div>
    <!-- Small Devices Header ends 
============================================= --> 
    
    <!-- Header 
============================================= -->
    <header id="dtr-header-global" class="fixed-top">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between"> 
                
                <!-- header left starts -->
                <div class="dtr-header-left"> 
                    
                    <!-- logo --> 
                    <a class="logo-default dtr-scroll-link" href="#home"><img src="<?php echo base_url(); ?>assets/images/logo-light.png" alt="logo"></a> 
                    
                    <!-- logo on scroll --> 
                    <a class="logo-alt dtr-scroll-link" href="#home"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
                    <!-- logo on scroll ends --> 
                    
                </div>
                <!-- header left ends --> 
                
                <!-- menu starts-->
                <div class="main-navigation navbar navbar-expand-lg ml-auto">
                    <ul class="dtr-scrollspy navbar-nav dtr-nav light-nav-on-load dark-nav-on-scroll">
                        <li class="nav-item"> <a class="nav-link active" href="#home">Accueil</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#about">Qui sommes-nous?</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#features">Passer à l'action</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="#news">News</a> </li>
                    </ul>
                </div>
                <!-- menu ends--> 
                
                <!-- header right starts -->
                <div class="dtr-header-right"> <a href="#donation" class="dtr-btn btn-rounded btn-orange dtr-scroll-link">S'abonner</a> </div>
                <!-- header right ends --> 
                
            </div>
        </div>
    </header>
    <!-- header ends
================================================== --> 
    
    <!-- == main content area starts == -->
    <div id="dtr-main-content"> 
        
        <!-- hero section starts
================================================== -->
        <section id="home" class="dtr-section hero-section-top-padding dtr-section-with-bg dtr-pb-100" style="background-image: url(<?php echo base_url(); ?>assets/images/hero-img3.jpg);">>
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 offset-md-6">
                        <div class="dtr-box dtr-rounded bg-white">
                            <h2 class="text-center">S'abonner</h2>
                            <p class="text-center text-grad-orange font-weight-700">à la bibliothèque en ligne</p>
                            
                            <!-- form starts -->
                            <div class="dtr-form">
                                <form id="contactform" method="post" action="php/contact-form.php">
                                    <fieldset>
                                        <div class="dtr-form-row dtr-form-row-2col">
                                            <p class="dtr-form-column">
                                                <input name="name"  type="text" placeholder="Nom(s)">
                                            </p>
                                            <p class="dtr-form-column">
                                                <input name="phone"  type="text" placeholder="Téléphone">
                                            </p>
                                        </div>
                                        <p>
                                            <input name="email"  class="required email" type="text" placeholder="Email">
                                        </p>
                                        <p class="antispam">Leave this empty: <br />
                                            <input name="url" />
                                        </p>
                                        <p>
                                            <textarea rows="6" name="message" id="message" class="required"  placeholder="Message"></textarea>
                                        </p>
                                        <p class="text-center">
                                            <button class="dtr-btn btn-rounded btn-blue" type="submit">Envoyer</button>
                                        </p>
                                        <div id="result"></div>
                                    </fieldset>
                                </form>
                            </div>
                            <!-- form ends -->
                            <p class="text-size-xs font-weight-600 text-center dtr-mt-20">* Vos informations personnelles sont confidentielles. <br>
                                Consultez notre politique de confidentialité pour plus d'informations. </p>
                        </div>
                    </div>
                    <!-- column 1 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- hero section ends
================================================== --> 
        
        <!-- section starts
================================================== -->
        <section id="about" class="dtr-section dtr-pt-100 dtr-pb-80 bg-blue">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <p class="text-size-xl color-white">Biblio+ est une librairie physique et numérique. Biblio+ Vous prête des livres pour lecture et les récupère après votre lecture avec son service de livraison à domicile. </p>
                        <span class="vert-line mt-20"></span>
                        <h2 class="color-white">Réjoignez-nous</h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- section ends
================================================== --> 
        
        <!-- section starts
================================================== -->
        <section class="dtr-section dtr-py-100">
            <div class="container"> 
                
                <!-- circle bg icon -->
                <div class="dtr-circle-icon bg-white color-orange"><i class="icon-mbri-down"></i></div>
                
                <!-- row 1 starts -->
                <div class="row align-items-center"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 dtr-md-mb-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img1.png" alt="image"> </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon1-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">close contact</span> with an infected person</p>
                        </div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour or randomised words which don't look even slightly believable. Lorem ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 1 ends --> 
                
                <!-- row 2 starts -->
                <div class="row align-items-center dtr-py-100"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon2-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">Droplets</span> spread when Infected
                                Person coughs or sneezes</p>
                        </div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour or randomised words which don't look even slightly believable. Lorem ipsum is simply dummy text of the printing and typesetting industry. </p>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 text-right dtr-md-mt-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img2.png" alt="image"> </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 2 ends --> 
                
                <!-- row 3 starts -->
                <div class="row align-items-center"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6 dtr-md-mb-30"> <img src="<?php echo base_url(); ?>assets/images/shape-img3.png" alt="image"> </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6">
                        <div class="clearfix dtr-mb-30"> <img src="<?php echo base_url(); ?>assets/images/icon3-64x64.png" alt="image" class="float-left dtr-mr-10">
                            <p class="text-size-lg font-weight-200 color-blue"><span class="font-weight-600 color-orange">Contact</span> with contaminated
                                surfaces or objects</p>
                        </div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour or randomised words which don't look even slightly believable. Lorem ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row 3 ends --> 
                
            </div>
        </section>
        <!-- section ends
================================================== --> 
        
        <!-- hashtag section starts
================================================== -->
        <section class="dtr-section hashtag-section-padding dtr-section-with-bg parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg.jpg);">
            <div class="dtr-overlay bg-dark-blue-trans"></div>
            <div class="container dtr-overlay-content">
                <div class="row">
                    <div class="col-12">
                        <h5 class="dtr-mb-30 color-white">together we’ll fight...</h5>
                        <p class="hashtag-heading color-white font-weight-700 wow fadeInUp">#go<span class="color-orange">corona</span>go</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- hashtag section ends
================================================== --> 
        
        <!-- features section starts
================================================== -->
        <section id="features" class="dtr-section dtr-py-100">
            <div class="container"> 
                
                <!-- section intro row starts -->
                <div class="row dtr-mb-30">
                    <div class="col-12 text-center">
                        <p class="text-grad-orange font-weight-700">Basic protective measures</p>
                        <h2>How to protect yourself</h2>
                    </div>
                </div>
                <!-- section intro row ends --> 
                
                <!-- row 1 starts -->
                <div class="row">
                    <div class="col-12 col-md-2 offset-md-1 text-center dtr-pt-100 dtr-mt-120 dtr-md-mt-0 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon1-care.png" alt="image" class="dtr-mb-30">
                        <h6>If you feel unwell get medical care early</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon2-care.png" alt="image" class="dtr-mb-30">
                        <h6>Avoid traveling if unnecessary</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-pt-20 dtr-md-mt-0"> <img src="<?php echo base_url(); ?>assets/images/icon3-care.png" alt="image" class="dtr-mb-30">
                        <h6>Wash your hands frequently</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon4-care.png" alt="image" class="dtr-mb-30">
                        <h6>Maintain social distancing</h6>
                    </div>
                    <div class="col-12 col-md-2 text-center dtr-pt-100 dtr-mt-120 dtr-md-mt-30 dtr-md-pt-0"> <img src="<?php echo base_url(); ?>assets/images/icon5-care.png" alt="image" class="dtr-mb-30">
                        <h6>Avoid touching eyes, nose and mouth</h6>
                    </div>
                </div>
                <!-- row 1 ends --> 
                
                <!-- row 2 starts -->
                <div class="row dtr-section-with-bg dtr-bg-size-auto dtr-bg-position-center-top dtr-mt-minus110 dtr-md-mt-30 wow fadeInUp" style="background-image: url(assets/images/curve.svg);">
                    <div class="col-12 col-md-6 offset-md-3 text-center dtr-pt-50">
                        <p class="dtr-mb-0 color-dark-blue">Be Ready to fight</p>
                        <p class="text-size-md-alt font-weight-600 dtr-mb-20 color-orange">#coronavirus</p>
                        <p class="color-dark-blue"> If you develop any symptoms such as fever,<br>
                            cough and difficulty breathing</p>
                        <p class="font-weight-700 dtr-mt-20 color-blue">Seek medical care immediately! </p>
                        <p class="text-size-xs font-weight-600 dtr-mt-30 color-dark-blue">Learn more to be ready for #COVID19:<br>
                            www.who.int/COVID19</p>
                    </div>
                </div>
                <!-- row 2 ends --> 
                
            </div>
        </section>
        <!-- features section ends
================================================== --> 
        
        <!-- testimonial section starts
================================================== -->
        <section class="dtr-section dtr-section-with-bg dtr-py-100 parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg1.jpg);"> 
            
            <!-- overlay -->
            <div class="dtr-overlay bg-dark-blue-trans"></div>
            <div class="container dtr-overlay-content"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6"> 
                        
                        <!-- heading-->
                        <h2 class="dtr-mb-30 color-white">What doctors<br>
                            have to say...</h2>
                        
                        <!--===== testimonial slider starts =====-->
                        <div class="dtr-slick-slider dtr-testimonial-style1"> 
                            
                            <!--== slide 1 starts ==-->
                            <div class="dtr-testimonial"> 
                                
                                <!-- review text -->
                                <div class="dtr-testimonial-content color-white">“I recommend to avoid close contact with anyone who is coughing.” </div>
                                <!-- client info -->
                                <div class="d-flex align-items-center dtr-client-info color-white">
                                    <div class="dtr-testimonial-user"> <img src="assets/images/user-1.jpg" alt="image"> </div>
                                    <div>
                                        <h6 class="dtr-client-name">Dr. Martha Weiner</h6>
                                        <span class="dtr-client-job">Acme Hospital, LA</span></div>
                                </div>
                            </div>
                            <!--== slide 1 ends ==--> 
                            
                            <!--== slide 2 starts ==-->
                            <div class="dtr-testimonial"> 
                                
                                <!-- review text -->
                                <div class="dtr-testimonial-content color-white">“ Greet people with a wave, a nod or bow instead.” </div>
                                <!-- client info -->
                                <div class="d-flex align-items-center dtr-client-info color-white">
                                    <div class="dtr-testimonial-user"> <img src="<?php echo base_url(); ?>assets/images/user-2.jpg" alt="image"> </div>
                                    <div>
                                        <h6 class="dtr-client-name">Dr. Alice Jones</h6>
                                        <span class="dtr-client-job">Acme Foundation, Ohio</span></div>
                                </div>
                            </div>
                            <!--== slide 2 ends ==--> 
                            
                            <!--== slide 3 starts ==-->
                            <div class="dtr-testimonial"> 
                                
                                <!-- review text -->
                                <div class="dtr-testimonial-content color-white">“Wash your hands with soap and running water.” </div>
                                <!-- client info -->
                                <div class="d-flex align-items-center dtr-client-info color-white">
                                    <div class="dtr-testimonial-user"> <img src="<?php echo base_url(); ?>assets/images/user-3.jpg" alt="image"> </div>
                                    <div>
                                        <h6 class="dtr-client-name">Dr. Marc Chapon</h6>
                                        <span class="dtr-client-job">Medical Research Labs, NY</span> </div>
                                </div>
                            </div>
                            <!--== slide 3 ends ==--> 
                            
                        </div>
                        <!--===== testimonial slider ends =====--> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-5 offset-md-1 dtr-md-mt-30"> 
                        
                        <!-- video box starts -->
                        <div class="dtr-video-box"> 
                            <!-- image --> 
                            <img src="<?php echo base_url(); ?>assets/images/video-bg.jpg" alt="image" class="dtr-rounded-big"> 
                            <!-- video button starts -->
                            <div class="color-white"> <a class="dtr-video-popup dtr-video-button bg-orange-trans" data-autoplay="true" data-vbtype="video" href="https://www.youtube.com/watch?v=kuceVNBTJio"></a> </div>
                            <!-- video button ends --> 
                        </div>
                        <!-- video box ends --> 
                        
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- testimonial section ends
================================================== --> 
        
        <!-- info section starts
================================================== -->
        <section class="dtr-section dtr-py-100 bg-grey">
            <div class="container"> 
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <div class="dtr-pr-30 dtr-sm-pr-0"><img src="<?php echo base_url(); ?>assets/images/image-1.png" alt="image"></div>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 dtr-md-mt-30">
                        <p class="text-size-lg font-weight-200 color-blue">Learn how you can <span class="font-weight-600 color-orange">survive</span> widespread COVID-19 outbreak</p>
                        <p class="text-grad-orange font-weight-700 dtr-mb-20">Educate yourself about 2019-nCoV</p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of lorem ipsum.</p>
                        <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. </p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row starts --> 
                
            </div>
        </section>
        <!-- info section ends
================================================== --> 
        
        <!-- services section starts
================================================== -->
        <section class="dtr-section dtr-section-with-bg dtr-py-100 parallax" style="background-image: url(<?php echo base_url(); ?>assets/images/section-bg2.jpg);"> 
            
            <!-- overlay -->
            <div class="dtr-overlay bg-blue-trans"></div>
            <div class="container dtr-overlay-content">
                <h2 class="text-center dtr-mb-40 color-white">How you can contribute?</h2>
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-4"> 
                        
                        <!-- icon box 1 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-users"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">Become volunteer</h4>
                                <p class="color-white">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat lorem ipsum.</p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">Become a volunteer</a> </div>
                        <!-- icon box 1 ends --> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- icon box 2 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-shopping-bag"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">Our webstore</h4>
                                <p class="color-white">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat lorem ipsum.</p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">Shop Now</a> </div>
                        <!-- icon box 2 ends --> 
                        
                    </div>
                    <!-- column 2 ends --> 
                    
                    <!-- column 3 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- icon box 3 starts -->
                        <div class="dtr-icon-box dtr-icon-box-rounded box-has-hover">
                            <div class="dtr-icon-box-icon bg-white color-orange"> <i class="icon-mbri-alert"></i> </div>
                            <div class="dtr-icon-box-content">
                                <h4 class="font-weight-200 color-white">Spread the word</h4>
                                <p class="color-white">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat lorem ipsum.</p>
                            </div>
                            <a href="#" class="dtr-icon-box-footer bg-white color-orange">Spread the word</a> </div>
                        <!-- icon box 3 ends --> 
                        
                    </div>
                    <!-- column 3 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- services section ends
================================================== --> 
        
        <!-- faq section starts
================================================== -->
        <section id="faq" class="dtr-section dtr-py-100 bg-grey">
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <p class="text-size-lg font-weight-200 color-blue">Your <span class="font-weight-600 color-orange">questions</span> answered</p>
                        <p class="text-grad-orange font-weight-700 dtr-mb-40">How to cope with 2019-nCoV outbreak</p>
                        
                        <!-- accordion 1 starts -->
                        <div class="dtr-accordion accordion" id="accord-index1"> 
                            
                            <!-- accordion tab 1 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading1">
                                    <h5 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link" type="button" data-toggle="collapse" data-target="#accord-index1-collapse1" aria-expanded="true" aria-controls="accord-index1-collapse1">What is COVID-9?</button>
                                    </h5>
                                </div>
                                <div id="accord-index1-collapse1" class="collapse show" aria-labelledby="accord-index1-heading1" data-parent="#accord-index1">
                                    <div class="card-body">Lorem text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Dummy texts have been in use by typesetters since the 16th century.</div>
                                </div>
                            </div>
                            <!-- accordion tab 1 ends --> 
                            
                            <!-- accordion tab 2 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading2">
                                    <h2 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button" data-toggle="collapse" data-target="#accord-index1-collapse2" aria-expanded="false" aria-controls="accord-index1-collapse2">Who is more at risk of severe illness?</button>
                                    </h2>
                                </div>
                                <div id="accord-index1-collapse2" class="collapse" aria-labelledby="accord-index1-heading2" data-parent="#accord-index1">
                                    <div class="card-body">Lorem text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Dummy texts have been in use by typesetters since the 16th century.</div>
                                </div>
                            </div>
                            <!-- accordion tab 2 ends --> 
                            
                            <!-- accordion tab 3 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading3">
                                    <h2 class="dtr-mb-0">
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button" data-toggle="collapse" data-target="#accord-index1-collapse3" aria-expanded="false" aria-controls="accord-index1-collapse3">What to do if you develop similar symptoms?</button>
                                    </h2>
                                </div>
                                <div id="accord-index1-collapse3" class="collapse" aria-labelledby="accord-index1-heading3" data-parent="#accord-index1">
                                    <div class="card-body">Lorem text is also used to demonstrate the appearance of different typefaces and layouts, and in general the content of dummy text is nonsensical. Dummy texts have been in use by typesetters since the 16th century.</div>
                                </div>
                            </div>
                            <!-- accordion tab 3 ends --> 
                            
                        </div>
                        <!-- accordion 1 ends --> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 text-right dtr-md-mt-30"> <img src="<?php echo base_url(); ?>assets/images/image-2.png" alt="image"> </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- faq section ends
================================================== --> 

        <!-- subscribe section starts
================================================== -->
        <section id="subscribe" class="dtr-section dtr-py-50">
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <p class="text-size-lg font-weight-300 color-blue">Obtenez toutes les actualités sur <span class="font-weight-600 color-orange">Biblio+</span> dans votre email. </p>
                    </div>
                    <!-- column 1 starts --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-5 offset-md-1 dtr-md-mt-30"> 
                        
                        <!-- form starts -->
                        <div class="subscribe-form-wrapper">
                            <form id="subscribeform" method="post" action="#">
                                <fieldset>
                                    <div class="subscribe-form-email">
                                        <input name="email"  class="required email" type="email" placeholder="Votre adresse email">
                                        <p class="subscribe-form-submit">
                                            <button type="submit" class="dtr-btn btn-rounded btn-blue"> Souscrire </button>
                                        </p>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- form ends -->
                        
                        <p class="text-size-xs-alt font-weight-400 dtr-mt-5 color-dark-blue">* Vos informations personnelles sont confidentielles. Consultez notre politique de confidentialité. </p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- subscribe section ends
================================================== --> 
        
        <!-- footer section starts
================================================== -->
        <footer id="dtr-footer">
            <div class="container">
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-sm-6 col-lg-3 dtr-mb-30"> <a href="index.html" class="d-block dtr-mb-30"><img src="assets/images/logo.png" alt="footer logo"></a>
                        <div class="dtr-social-large">
                            <ul class="dtr-social dtr-social-list social-light text-left">
                                <li><a href="#" class="dtr-linkedin color-grey" target="_blank" title="linkedin"></a></li>
                                <li><a href="#" class="dtr-facebook color-grey " target="_blank" title="facebook"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-sm-6 col-lg-5 dtr-mb-30">
                        <h6>Biblio+</h6>
                        <p>Vous prête des livres pour lecture et les récupère après votre lecture avec son service de livraison à domicile ...</p>
                        <p class="text-size-xs dtr-mt-20 dtr-mb-30">copyright© 2020 by <a href="http://t7ch.com" target="_blank">t7ch</a>.<br>
                            All rights reserved.</p>
                    </div>
                    <!-- column 2 ends --> 
                     
                    
                    <!-- column 4 starts -->
                    <div class="col-12 col-sm-6 col-lg-4 dtr-mb-30">
                        <h6>Contactez-nous</h6>
                        <p class="d-flex dtr-mb-20"><i class="icon-envelope1 dtr-mt-5 dtr-mr-15"></i><span><a href="mailto:contact@example.com">info@biblioplus.com</a></span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-phone-alt dtr-mt-5 dtr-mr-15"></i><span>+225 47 24 66 59</span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-map-marker-alt dtr-mt-5 dtr-mr-15"></i><span>Riviera 2,
                            Abidjan,<br>
                            Côte d'Ivoire</span></p>
                    </div>
                    <!-- column 4 ends --> 
                    
                </div>
            </div>
        </footer>
        <!-- footer section ends
================================================== --> 
        
    </div>
    <!-- == main content area ends == --> 
    
</div>
<!-- #dtr-wrapper ends --> 

<!-- JS FILES --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>