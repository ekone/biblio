<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<title>Biblio+, Bibliothèque et Formations en ligne</title>
<meta name="author" content="tansh">
<meta name="description" content="HTML Teamplate">
<meta name="keywords" content="Bibliothèque, Formations">

<!-- FAVICON FILES -->
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon" sizes="144x144">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-120-precomposed.png" rel="apple-touch-icon" sizes="120x120">
<link href="<?php echo base_url(); ?>assets/images/icons/apple-touch-icon-76-precomposed.png" rel="apple-touch-icon" sizes="76x76">
<link href="<?php echo base_url(); ?>assets/images/icons/favicon.png" rel="shortcut icon">

<!-- CSS FILES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/iconfonts.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color-blue.css">
</head>
<body>
<div id="dtr-wrapper" class="clearfix"> 
    
    <!-- preloader starts -->
    <div class="dtr-preloader">
        <div class="dtr-preloader-inner">
            <div class="dtr-preloader-img"></div>
        </div>
    </div>
    <!-- preloader ends --> 
    
    <!-- Small Devices Header 
============================================= -->
    <div class="dtr-responsive-header fixed-top">
        <div class="container"> 
            
            <!-- small devices logo --> 
            <a href="index.html"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
            <!-- small devices logo ends --> 
            
            <!-- menu button -->
            <button id="dtr-menu-button" class="dtr-hamburger" type="button"><span class="dtr-hamburger-lines-wrapper"><span class="dtr-hamburger-lines"></span></span></button>
        </div>
        <div class="dtr-responsive-header-menu"></div>
    </div>
    <!-- Small Devices Header ends 
============================================= --> 
    
    <!-- Header 
============================================= -->
    <header id="dtr-header-global" class="fixed-top">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between"> 
                
                <!-- header left starts -->
                <div class="dtr-header-left"> 
                    
                    <!-- logo --> 
                    <a class="logo-default dtr-scroll-link" href="<?php echo site_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo-light.png" alt="logo"></a> 
                    
                    <!-- logo on scroll --> 
                    <a class="logo-alt dtr-scroll-link" href="<?php echo site_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a> 
                    <!-- logo on scroll ends --> 
                    
                </div>
                <!-- header left ends --> 
                
                <!-- menu starts-->
                <div class="main-navigation navbar navbar-expand-lg ml-auto">
                    <ul class="dtr-scrollspy navbar-nav dtr-nav light-nav-on-load dark-nav-on-scroll">
                        <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>#home">Accueil</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url(); ?>#about">Qui sommes-nous?</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url(); ?>#features">Passer à l'action</a> </li>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url(); ?>#news">News</a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="<?php echo site_url('boutique'); ?>">La boutique</a> </li>
                    </ul>
                </div>
                <!-- menu ends--> 
                
                <!-- header right starts -->
                <div class="dtr-header-right"> <a href="#donation" class="dtr-btn btn-rounded btn-orange dtr-scroll-link">S'abonner</a> </div>
                <!-- header right ends --> 
                
            </div>
        </div>
    </header>
    <!-- header ends
================================================== --> 
    
    <!-- == main content area starts == -->
    <div id="dtr-main-content"> 
        
        <!-- hero section starts
================================================== -->
        <section id="home" class="dtr-section hero-section-top-padding dtr-section-with-bg dtr-pb-100" style="background-image: url(<?php echo base_url(); ?>assets/images/hero-img3.jpg);">>
            <div class="container"> 
                
            </div>
        </section>
        <!-- hero section ends
================================================== --> 

                <!-- news section starts
================================================== -->
<section id="news" class="dtr-section dtr-py-100 bg-grey">
            <div class="container"> 
                
                <!-- section intro row starts -->
                <div class="row dtr-mb-30">
                    <div class="col-12 text-center">
                        <h2>Nos livres</h2>
                        <p class="text-grad-orange font-weight-700">Développement personnel, Sciences, Finance, Réligion, ...</p>
                    </div>
                </div>
                <!-- section intro row ends --> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-4"> 
                        
                        <!-- blog item 1 starts -->
                        <div class="dtr-blog-item"> 
                            <!-- image -->
                            <div class="dtr-post-img"> <img src="<?php echo base_url(); ?>assets/images/livres/001_a-qui-la-faute.jpg" alt="image" class="dtr-rounded"> </div>
                            <p class="text-size-sm font-weight-500 color-blue"><span class="dtr-date">Maurice D. KOUE</span></p>
                            <h5><a href="#">A Qui la faute ?</a></h5>
                        </div>
                        <!-- blog item 1 ends --> 
                        
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- blog item 2 starts -->
                        <div class="dtr-blog-item"> 
                            <!-- image -->
                            <div class="dtr-post-img"> <img src="<?php echo base_url(); ?>assets/images/livres/002_l-ordonnance.jpg" alt="image" class="dtr-rounded"> </div>
                            <p class="text-size-sm font-weight-500 color-blue"><span class="dtr-date">SORO Guéfala</span></p>
                            <h5><a href="#">L'ordonnance</a></h5>
                        </div>
                        <!-- blog item 2 ends --> 
                        
                    </div>
                    <!-- column 2 ends --> 
                    
                    <!-- column 3 starts -->
                    <div class="col-12 col-md-4 dtr-md-mt-30"> 
                        
                        <!-- blog item 3 starts -->
                        <div class="dtr-blog-item"> 
                            <!-- image -->
                            <div class="dtr-post-img"> <img src="<?php echo base_url(); ?>assets/images/livres/003_ma-parole-pour-le-noir-aux-lumieres-eteintes.jpg" alt="image" class="dtr-rounded"> </div>
                            <p class="text-size-sm font-weight-500 color-blue"><span class="dtr-date">Serge BILE</span></p>
                            <h5><a href="#">Ma parole pour le noir aux lumieres eteintes</a></h5>
                        </div>
                        <!-- blog item 3 ends --> 
                        
                    </div>
                    <!-- column 3 ends --> 
                    
                </div>
                <!-- row starts --> 
                
            </div>
        </section>
        <!-- news section ends
================================================== --> 

        <!-- subscribe section starts
================================================== -->
        <section id="subscribe" class="dtr-section dtr-py-50">
            <div class="container"> 
                
                <!-- row starts -->
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">
                        <p class="text-size-lg font-weight-300 color-blue">Obtenez toutes les actualités sur <span class="font-weight-600 color-orange">Biblio+</span> dans votre email. </p>
                    </div>
                    <!-- column 1 starts --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-md-5 offset-md-1 dtr-md-mt-30"> 
                        
                        <!-- form starts -->
                        <div class="subscribe-form-wrapper">
                            <form id="subscribeform" method="post" action="#">
                                <fieldset>
                                    <div class="subscribe-form-email">
                                        <input name="email"  class="required email" type="email" placeholder="Votre adresse email">
                                        <p class="subscribe-form-submit">
                                            <button type="submit" class="dtr-btn btn-rounded btn-blue"> Souscrire </button>
                                        </p>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- form ends -->
                        
                        <p class="text-size-xs-alt font-weight-400 dtr-mt-5 color-dark-blue">* Vos informations personnelles sont confidentielles. Consultez notre politique de confidentialité. </p>
                    </div>
                    <!-- column 2 ends --> 
                    
                </div>
                <!-- row ends --> 
                
            </div>
        </section>
        <!-- subscribe section ends
================================================== --> 
        
        <!-- footer section starts
================================================== -->
        <footer id="dtr-footer">
            <div class="container">
                <div class="row"> 
                    
                    <!-- column 1 starts -->
                    <div class="col-12 col-sm-6 col-lg-3 dtr-mb-30"> <a href="index.html" class="d-block dtr-mb-30"><img src="assets/images/logo.png" alt="footer logo"></a>
                        <div class="dtr-social-large">
                            <ul class="dtr-social dtr-social-list social-light text-left">
                                <li><a href="#" class="dtr-linkedin color-grey" target="_blank" title="linkedin"></a></li>
                                <li><a href="#" class="dtr-facebook color-grey " target="_blank" title="facebook"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- column 1 ends --> 
                    
                    <!-- column 2 starts -->
                    <div class="col-12 col-sm-6 col-lg-5 dtr-mb-30">
                        <h6>Biblio+</h6>
                        <p>Vous prête des livres pour lecture et les récupère après votre lecture avec son service de livraison à domicile ...</p>
                        <p class="text-size-xs dtr-mt-20 dtr-mb-30">copyright© 2020 by <a href="http://t7ch.com" target="_blank">t7ch</a>.<br>
                            All rights reserved.</p>
                    </div>
                    <!-- column 2 ends --> 
                     
                    
                    <!-- column 4 starts -->
                    <div class="col-12 col-sm-6 col-lg-4 dtr-mb-30">
                        <h6>Contactez-nous</h6>
                        <p class="d-flex dtr-mb-20"><i class="icon-envelope1 dtr-mt-5 dtr-mr-15"></i><span><a href="mailto:contact@example.com">info@biblioplus.com</a></span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-phone-alt dtr-mt-5 dtr-mr-15"></i><span>+225 47 24 66 59</span></p>
                        <p class="d-flex dtr-mb-20"><i class="icon-map-marker-alt dtr-mt-5 dtr-mr-15"></i><span>Riviera 2,
                            Abidjan,<br>
                            Côte d'Ivoire</span></p>
                    </div>
                    <!-- column 4 ends --> 
                    
                </div>
            </div>
        </footer>
        <!-- footer section ends
================================================== --> 
        
    </div>
    <!-- == main content area ends == --> 
    
</div>
<!-- #dtr-wrapper ends --> 

<!-- JS FILES --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>