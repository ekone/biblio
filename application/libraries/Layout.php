<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//Cette classe permet de charger nos vues dans la vue de default
class Layout
{
	private $CI;
	private $output		= '';
	
	//Constructeur
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function admin($name, $data = array())
	{
		$this->output .= $this->CI->load->view('admin/'.$name, $data,true);
		$this->CI->load->view('admin/default', array('output'	=> $this->output));
	}
	
	public function pages_accueil($name, $data = array())
	{
		$this->output .= $this->CI->load->view($name, $data,true);
		$this->CI->load->view('default_accueil', array('output'	=> $this->output));
	}
	
	public function pages($name, $data = array())
	{
		$this->output .= $this->CI->load->view($name, $data,true);
		$this->CI->load->view('fr/default', array('output'	=> $this->output));
	}

	public function pages_en($name, $data = array())
	{
		$this->output .= $this->CI->load->view($name, $data,true);
		$this->CI->load->view('en/default', array('output'	=> $this->output));
	}
}

/* End of file layout.php */
/* Location: ./application/libraries/layout.php */
?>